---
layout: page
title: WebSites
permalink: "/websites/"
---

### Work Web Sites Affiliated / Coded (Intranet - Extranet)

<p> - <a href="https://www.harris.com/"> Harris Corp. GCSD </a></p>

<p> - <a href="https://www.asco.com/en-us"> Emerson / ASCO Valve </a></p>

<p> - <a href="https://bssinspires.com/"> BSSInspires </a></p>

<p> - <a href="http://www.wired4health.net/"> Wired4Health </a></p>

<p><a href="https://www.sdes.ucf.edu/"> University of Central Florida (SDES Student Development and Enrollment Services)</a></p>


### EDUCATION

BA - Computer Technology, Martin University
