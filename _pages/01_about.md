---
layout: page
title: About
permalink: /about/
---

  I have worked in education, government, insurance, nonprofit and health industries. I consider myself to be a results-oriented, dependable, and capable worker; who works well with others. I am a fast learner in any on the job situation, if required.

  My education in Computer Technology B.A. and on-the-job applications experience and practical skills primarily in software development using Php, .Net, WordPress and Ajax contribute to my UI / front development work. As well my experience in relational database management using SQL Server, SQL, MySql query development and REST/Api support my backend skills. I also have proficient skills with website design, HTML, CSS, JQuery, Bootstrap and JavaScript development, as well as website administration.

  Need more details or have questions just send an e-mail or phone and I'll get back to you.

