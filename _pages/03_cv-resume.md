---
layout: page
title: CV-Resume
permalink: "/cv-resume/"
---

### EXPERIENCE

<p><u>Self-employed (Freelance/Contractor)</u>                               <b>Jul 2016 to Now</b></p>

 - Development of documentation and examples for Vue.js, Git, JScript, Node.js, ES5
 - Development of JavaScript, JQuery, HTML, CSS calendar plugin web application
 - Php open source code to support health care clients using MySQL, JSON, LAMP.
 - Developed front end UI (HTML 5, Bootstrap), WordPress, CodeIgniter, RoR
 - Built web sites for Taproot using HTML, Sass, Less, JavaScript, CSS3, Ajax, Responsee

<p><u>Wired4Health (Remote Contractor)</u>                                   <b>Oct 2015 to Apr 2016</b></p>

 - Development of SQL, Ruby / Ruby on Rails applications
 - Php code LAMP to support health care clients (hospital, pharmacy, HMO) data
 - Participated in client testing of data loads and their client reviews
 - Used and loaded Oracle, PostgreSQL databases.

<p><u>BSSInpsires.com (Developer)</u>                                        <b>Feb 2015 to Mar 2015</b></p>

 - Development of CRUD applications within the client’s domain.
 - Front end (Jquery, HTML, CSS) and backend enhancements to MySQL.
 - LAMP, Jquery, JavaScript, Ajax, CSS, HTML, REST and BootStrap tools.

<p><u>CCI (Remote Developer)</u>                                             <b>Jun 2013 to Feb 2015</b></p>

 - Development and maintenance of .NET / Java web sites JavaScript, HTML, CSS, Jquery
 - Used MS Visual Studio, BootStrap and SQL Server 2008r2.
 - Maintained web applications for account payable, invoicing, receivables
 - General CRUD applications within the client’s domain (Emerson Industrial Automation).

<p><u>Computer Science Raytheon (Developer)</u>                              <b>Mar 2012 to Oct 2012</b></p>

 - Development and maintenance of .NET web sites US Air Force for launch w/ULA.
 - MS Excel, middle tier objects SQL, queries development SQL Server 2008
 - Implemented new web pages to report real-time data from launches

<p><u>Yoh Consulting (Contractor)</u>                                        <b>Mar 2011 to Mar 2012</b></p>

 - Development and maintenance of web sites for Harris Communications.
 - Maintained parts and search system / tracking application Java / CFML.
 - Developed conference room, publishing, word and acronym look-ups web sites.
 - Oracle, REST, MS SQL, SQL, Ajax, Jquery, HTML and JavaScript tools.

<p><u>UCF (Web Developer University of Central Florida)</u>                  <b>Sep 2008 to Mar 2011</b></p>

 - Development & maintenance of .Net, SQL Server and LAMP PHP, MySQL websites
 - Setup SharePoint for SDES division with site administrator & InfoPath development.
 - Maintenance to Redmine project management tool in Ruby on Rails.

### SKILLS

Php, LAMP, JQuery, HTML, CSS, MySQL, Ajax, JavaScript, Bootstrap, SQL, SQL Server, Ruby/Rails (RoR), Sublime, WordPress, Joomla, Git, MVC, Java, NodeJS, TDD, Atom IDE, Flask, REST, .Net

### EDUCATION

BA - Computer Technology, Martin University
