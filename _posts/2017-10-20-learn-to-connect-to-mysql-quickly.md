---
layout: post
title: Learn to Connect to MySQL Quickly
categories: MySQL
---

Learn to Connect to MySQL Quickly

Very simple define your connection string then create DSN to create a PDO connection object.

<pre>
// Database details
$db_server   = 'localhost';
$db_username = 'root';
$db_password = 'rootlh';
$db_name     = 'company';

$dsn = "mysql:host=$db_server;dbname=$db_name;charset=utf8";
$opt = [
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
  PDO::ATTR_EMULATE_PREPARES   => false
];

$pdo = new PDO($dsn, $db_username, $db_password, $opt);

</pre>
